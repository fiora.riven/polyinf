from PySide2.QtWidgets import QApplication, QDialog, QFormLayout, QLineEdit, QPushButton, QComboBox
from PySide2 import QtGui
from sql_workspace import config
import mysql.connector

class InsertDataDialog(QDialog):
    def __init__(self, database, table, parent=None):
        super().__init__(parent)

        self.database = database
        self.table = table
        self.columns, self.kolone = self.get_table_columns(database, table)
        self.data = {}
        print(self.columns)

        self.setWindowTitle("Insert Data")
        self.setWindowIcon(QtGui.QIcon("resources/icons/app-icon2.png"))
        layout = QFormLayout()
        self.line_edits = []



        for column in self.columns:
            if column in self.kolone:
                data = self.get_combo_data(database, table, column)
                combo = QComboBox()
                combo.addItems(data)
                layout.addRow(column, combo)
            else:
                line_edit = QLineEdit()
                layout.addRow(column, line_edit)
                self.line_edits.append(line_edit)

        submit_button = QPushButton("Submit")
        submit_button.clicked.connect(self.submit_data)
        layout.addRow(submit_button)
        self.setLayout(layout)

    def submit_data(self):
        line_edit_index = 0  # Index for line edits
        for index, column in enumerate(self.columns):
            if column in self.kolone:
                combo_box = self.layout().itemAt(index, QFormLayout.FieldRole).widget()
                value = combo_box.currentText()
            else:
                line_edit = self.line_edits[line_edit_index]
                value = line_edit.text().strip()
                line_edit_index += 1

            if not value:
                if column in self.kolone:
                    combo_box.setStyleSheet("border: 1px solid red;")
                else:
                    line_edit.setStyleSheet("border: 1px solid red;")
                return

            self.data[column] = value

        self.insert_data()

    def insert_data(self):
        myconfig = config.load_config("myconfig.json")
        conn = mysql.connector.connect(
            host=myconfig["host"],
            user=myconfig["user"],
            password=myconfig["password"],
            db=self.database
        )
        cursor = conn.cursor()

        columns = ', '.join(self.data.keys())
        values = ', '.join(['%s'] * len(self.data.values()))

        query = f"INSERT INTO {self.table} ({columns}) VALUES ({values})"
        data_values = tuple(self.data.values())

        cursor.execute(query, data_values)
        conn.commit()

        cursor.close()
        conn.close()

        self.accept()

    
    def get_table_columns(self, database, table):
        myconfig = config.load_config("myconfig.json")
        conn = mysql.connector.connect(
            host=myconfig["host"],
            user=myconfig["user"],
            password=myconfig["password"],
            db=database
        )
        cursor = conn.cursor(buffered=True)
        
        query = f"DESCRIBE {table}"
        cursor.execute(query)
        
        columns = [column[0] for column in cursor.fetchall()]


        query = "SELECT VezaneKod FROM vezanetabele WHERE VezTKod = %s"
        cursor.execute(query, (table,))
        kolone = [column[0] for column in cursor.fetchall()]
        
        cursor.close()
        conn.close()

        return columns, kolone
    


    def get_combo_data(self, database, table, kolona):
        myconfig = config.load_config("myconfig.json")
        conn = mysql.connector.connect(
            host=myconfig["host"],
            user=myconfig["user"],
            password=myconfig["password"],
            db=database
        )
        cursor = conn.cursor(buffered=True)


        query = "SELECT OsnTKodIme FROM vezanetabele WHERE VezTKod = %s"
        cursor.execute(query, (table,))
        tabela = cursor.fetchall()


        query = "SELECT OsnvKod FROM vezanetabele WHERE VezTKod = %s AND VezaneKod = %s"
        cursor.execute(query, (table, kolona))
        item = cursor.fetchone()

        for tabela_item in tabela:
            try:
                query = "SELECT {} FROM {}".format(item[0], tabela_item[0])
                cursor.execute(query)
                data = list(set(str(column[0]) for column in cursor.fetchall() if column[0] is not None))
                return data
                # Do something with the data for the current tabela_item
            except mysql.connector.errors.ProgrammingError:
                # Handle the case where the item does not exist in the table
                print("Item does not exist in the table:", tabela_item)

        
        