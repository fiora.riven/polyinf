from PySide2 import QtWidgets
from bson import ObjectId
from pymongo import MongoClient
import mysql.connector
import mysql.connector
from arango import ArangoClient

class ArangoOptionsDialog(QtWidgets.QDialog):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Choose an Option")
        options = [
            "Bogdan Graf",
            "Nemanja Graf",
            "Nina Graf",
            "Dusan Graf",
            "Lena Graf",
            "Mihajlo Graf"
        ]

        self.optionComboBox = QtWidgets.QComboBox()
        self.optionComboBox.addItems(options)

        submitButton = QtWidgets.QPushButton("Submit")
        submitButton.clicked.connect(self.handle_submit)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.optionComboBox)
        layout.addWidget(submitButton)
        self.setLayout(layout)

    def handle_submit(self):
        selected_option = self.optionComboBox.currentText()

        if selected_option == "Bogdan Graf":
            self.bogdan_dokument()
        elif selected_option == "Nemanja Graf":
            self.nemanja_dokument()
        elif selected_option == "Nina Graf":
            self.nina_dokument()
        elif selected_option == "Dusan Graf":
            self.dusan_dokument()
        elif selected_option == "Lena Graf":
            self.lena_dokument()
        elif selected_option == "Mihajlo Graf":
            self.mihajlo_dokument()

    def bogdan_dokument(self):
        dialog = QtWidgets.QDialog(self)
        dialog.setWindowTitle("Bogdan Graf")

        layout = QtWidgets.QFormLayout()
        verzijaLineEdit = QtWidgets.QLineEdit()
        kataloskiBrojLineEdit = QtWidgets.QLineEdit()
        drzavaLineEdit = QtWidgets.QLineEdit()
        kompanijaLineEdit = QtWidgets.QLineEdit()
        layout.addRow("Verzija:", verzijaLineEdit)
        layout.addRow("Kataloski Broj:", kataloskiBrojLineEdit)
        layout.addRow("Drzava:", drzavaLineEdit)
        layout.addRow("ID kompanije:", kompanijaLineEdit)

        submitButton = QtWidgets.QPushButton("Submit")
        submitButton.clicked.connect(dialog.accept)
        layout.addRow(submitButton)

        dialog.setLayout(layout)
        dialog.exec_()

        if dialog.result() == QtWidgets.QDialog.Accepted:
            verzija = verzijaLineEdit.text()
            kataloski_broj = kataloskiBrojLineEdit.text()
            drzava = drzavaLineEdit.text()
            kompanija = kompanijaLineEdit.text()



            # MySQL configuration
            mysql_host = 'localhost'
            mysql_port = 3306
            mysql_user = 'root'
            mysql_password = 'root'
            mysql_database = 'nosql'

            # ArangoDB configuration
            arango_host = 'http://localhost:8529'
            arango_database = '_system'

            # Connect to MySQL
            mysql_conn = mysql.connector.connect(
                host=mysql_host,
                port=mysql_port,
                user=mysql_user,
                password=mysql_password,
                database=mysql_database
            )

            mysql_cursor = mysql_conn.cursor()

            # Connect to ArangoDB
            arango_client = ArangoClient(hosts=arango_host)
            db = arango_client.db(arango_database)

            # Drop collections if they already exist
            if db.has_collection('complexProducts'):
                db.delete_collection('complexProducts')

            if db.has_collection('versions'):
                db.delete_collection('versions')

            if db.has_collection('softwareProducts'):
                db.delete_collection('softwareProducts')

            if db.has_collection('containsEdges'):
                db.delete_collection('containsEdges')

            # Create collections
            complex_products = db.create_collection('complexProducts', edge=False)
            versions = db.create_collection('versions', edge=False)
            software_products = db.create_collection('softwareProducts', edge=False)
            contains_edges = db.create_collection('containsEdges', edge=True)

            # Execute the MySQL procedure to populate the temporary table
            mysql_cursor.execute("DROP PROCEDURE IF EXISTS bogdan_dokument")

# Create the procedure
            mysql_cursor.execute("""
            CREATE DEFINER = `root`@`localhost` PROCEDURE `bogdan_dokument` (IN ver INT, katbr char(10), dr_oznaka char(4), ps_id int)
BEGIN 
    DROP TEMPORARY TABLE IF EXISTS BOGDAN_DOKUMENT;

CREATE TEMPORARY TABLE BOGDAN_DOKUMENT(
 SP_DR_OZNAKA char(4), 
 SP_POS_PS_ID int, 
 SP_SPR_KATBR char(10),
 SP_SPR_TIP char(1),
 SP_SPR_NAZIV varchar(120), 
 VER_VER_SOF_DR_OZNAKA char(4), 
 VER_VER_POS_PS_ID int, 
 VER_SPR_KATBR char(10), 
 VER_SPR_TIP char(1), 
 VER_VER_OZNAKA int, 
 SA_SOF_DR_OZNAKA char(4), 
 SA_POS_PS_ID int, 
 SA_KON_SPR_KATBR char(10), 
 SA_KON_SPR_TIP char(1), 
 SA_KON_VER_OZNAKA int, 
 SA_KONF_ID int, 
 SA_SASTAV_RBR smallint, 
 SA_VER_SOF_DR_OZNAKA char(4), 
 SA_VER_POS_PS_ID int, 
 SA_SPR_KATBR char(10), 
 SA_SPR_TIP char(1), 
 SA_VER_OZNAKA int
 );
    INSERT INTO BOGDAN_DOKUMENT(
    SP_DR_OZNAKA, 
    SP_POS_PS_ID, 
    SP_SPR_KATBR,
    SP_SPR_TIP,
    SP_SPR_NAZIV,

    VER_VER_SOF_DR_OZNAKA, 
    VER_VER_POS_PS_ID, 
    VER_SPR_KATBR, 
    VER_SPR_TIP, 
    VER_VER_OZNAKA, 
    
    SA_SOF_DR_OZNAKA, 
    SA_POS_PS_ID, 
    SA_KON_SPR_KATBR, 
    SA_KON_SPR_TIP, 
    SA_KON_VER_OZNAKA, 
    SA_KONF_ID, 
    SA_SASTAV_RBR, 
    SA_VER_SOF_DR_OZNAKA, 
    SA_VER_POS_PS_ID, 
    SA_SPR_KATBR, 
    SA_SPR_TIP, 
    SA_VER_OZNAKA
    )
SELECT
    SP.DR_OZNAKA,
    SP.POS_PS_ID,
    SP.SPR_KATBR,
    SP.SPR_TIP,
    SP.SPR_NAZIV,
    V.SOF_DR_OZNAKA,
    V.POS_PS_ID,
    V.SPR_KATBR,
    V.SPR_TIP,
    V.VER_OZNAKA,
    S.SOF_DR_OZNAKA,
    S.POS_PS_ID,
    S.KON_SPR_KATBR,
    S.KON_SPR_TIP,
    S.KON_VER_OZNAKA,
    S.KONF_ID,
    S.SASTAV_RBR,
    S.VER_SOF_DR_OZNAKA,
    S.VER_POS_PS_ID,
    S.SPR_KATBR,
    S.SPR_TIP,
    S.VER_OZNAKA
FROM softverski_proizvod SP, verzija V, sastav S
WHERE SP.DR_OZNAKA = V.SOF_DR_OZNAKA
    AND SP.POS_PS_ID = V.POS_PS_ID
    AND SP.SPR_KATBR = V.SPR_KATBR
    AND SP.SPR_TIP = V.SPR_TIP
    AND V.SOF_DR_OZNAKA = S.SOF_DR_OZNAKA
    AND V.POS_PS_ID = S.POS_PS_ID
    AND V.SPR_KATBR = S.KON_SPR_KATBR
    AND V.SPR_TIP = S.KON_SPR_TIP
    AND V.VER_OZNAKA = S.KON_VER_OZNAKA
    AND SP.SPR_KATBR = katbr
    AND V.VER_OZNAKA = ver
    AND SP.DR_OZNAKA = dr_oznaka
    AND SP.POS_PS_ID = ps_id
    AND EXISTS (
        SELECT 1
        FROM sastav S2
        WHERE S2.SOF_DR_OZNAKA = SP.DR_OZNAKA
            AND S2.POS_PS_ID = SP.POS_PS_ID
            AND S2.KON_SPR_KATBR = SP.SPR_KATBR
            AND S2.KON_SPR_TIP = SP.SPR_TIP
            AND S2.KON_VER_OZNAKA = V.VER_OZNAKA
    );
    SELECT * FROM BOGDAN_DOKUMENT;
END

            """)
            mysql_cursor.callproc('bogdan_dokument', [verzija, kataloski_broj, drzava, kompanija]) 
            mysql_cursor.execute("SELECT * FROM BOGDAN_DOKUMENT")
            bogdan_dokument_rows = mysql_cursor.fetchall()

            # Map of MySQL IDs to ArangoDB document IDs
            document_id_map = {}

            # Insert complex products
            for row in bogdan_dokument_rows:
                complex_product = {
                    'SP_DR_OZNAKA': row[0],
                    'SP_POS_PS_ID': row[1],
                    'SP_SPR_KATBR': row[2],
                    'SP_SPR_TIP': row[3],
                    'SP_SPR_NAZIV': row[4],
                }
                document_id = complex_products.insert(complex_product, return_new=True)['_id']
                document_id_map[(row[0], row[1])] = document_id

            # Insert versions and software products
            for row in bogdan_dokument_rows:
                complex_product_id = document_id_map[(row[0], row[1])]

                version = {
                    'VER_VER_SOF_DR_OZNAKA': row[5],
                    'VER_VER_POS_PS_ID': row[6],
                    'VER_SPR_KATBR': row[7],
                    'VER_SPR_TIP': row[8],
                    'VER_VER_OZNAKA': row[9],
                }
                document_id = versions.insert(version, return_new=True)['_id']
                version_id = document_id 
                document_id_map[(row[5], row[6], row[7], row[8], row[9])] = version_id

                software_product = {
                    'SA_SOF_DR_OZNAKA': row[10],
                    'SA_POS_PS_ID': row[11],
                    'SA_KON_SPR_KATBR': row[12],
                    'SA_KON_SPR_TIP': row[13],
                    'SA_KON_VER_OZNAKA': row[14],
                    'SA_KONF_ID': row[15],
                    'SA_SASTAV_RBR': row[16],
                    'SA_VER_SOF_DR_OZNAKA': row[17],
                    'SA_VER_POS_PS_ID': row[18],
                    'SA_SPR_KATBR': row[19],
                    'SA_SPR_TIP': row[20],
                    'SA_VER_OZNAKA': row[21]
                }
                document_id = software_products.insert(software_product, return_new=True)['_id']
                software_product_id = document_id
                document_id_map[(row[10], row[11], row[12], row[13], row[14])] = software_product_id

                # Create edges
                contains_edges.insert(
                    {
                        '_from': complex_product_id,
                        '_to': version_id
                    }
                )

                contains_edges.insert(
                    {
                        '_from': version_id,
                        '_to': software_product_id
                    }
                )

            print('Data imported successfully.')


    def nemanja_dokument(self):
        dialog = QtWidgets.QDialog(self)
        dialog.setWindowTitle("Nemanja Graf")

        layout = QtWidgets.QFormLayout()
        slozena_aktivnostLineEdit = QtWidgets.QLineEdit()
        
        layout.addRow("Slozena Aktivnost:", slozena_aktivnostLineEdit)
        
        submitButton = QtWidgets.QPushButton("Submit")
        submitButton.clicked.connect(dialog.accept)
        layout.addRow(submitButton)

        dialog.setLayout(layout)
        dialog.exec_()

        if dialog.result() == QtWidgets.QDialog.Accepted:
            slozena_aktivnost = slozena_aktivnostLineEdit.text()
            

                        # MySQL configuration

            mysql_host = 'localhost'

            mysql_port = 3306

            mysql_user = 'root'

            mysql_password = 'root'

            mysql_database = 'nosql'




            # ArangoDB configuration

            arango_host = 'http://localhost:8529'

            arango_database = '_system'




            # Connect to MySQL

            mysql_conn = mysql.connector.connect(

                host=mysql_host,

                port=mysql_port,

                user=mysql_user,

                password=mysql_password,

                database=mysql_database

            )




            mysql_cursor = mysql_conn.cursor()

            mysql_cursor.execute("DROP PROCEDURE IF EXISTS nemanja_dokument")
            mysql_cursor.execute("""




                CREATE DEFINER = `root`@`localhost` PROCEDURE `nemanja_dokument` (IN P1 CHAR(2))
BEGIN
    DROP TEMPORARY TABLE IF EXISTS NEMANJA_DOKUMENT;

    CREATE TEMPORARY TABLE NEMANJA_DOKUMENT (
        AKT_OZNAKA CHAR(2),
        AKT_NAZIV VARCHAR(240),
        SA_AKT_OZNAKA CHAR(2),
        SA_AKT_VERZIJA SMALLINT,
        SUV_STR_AKT_OZNAKA CHAR(2),
        SUV_AKT_OZNAKA CHAR(2),
        SUV_AKT_VERZIJA SMALLINT
    );

    INSERT INTO NEMANJA_DOKUMENT (
        AKT_OZNAKA,
        AKT_NAZIV,
        SA_AKT_OZNAKA,
        SA_AKT_VERZIJA,
        SUV_STR_AKT_OZNAKA,
        SUV_AKT_OZNAKA,
        SUV_AKT_VERZIJA
    )
    SELECT
        ka.AKT_OZNAKA,
        ka.AKT_NAZIV,
        sa.AKT_OZNAKA,
        sa.AKT_VERZIJA,
        suv.STR_AKT_OZNAKA,
        suv.AKT_OZNAKA,
        suv.AKT_VERZIJA
    FROM
        katalog_aktivnosti ka,
        struktura_aktivnosti sa,
        sastav__u_verziji suv
    WHERE
        ka.AKT_OZNAKA = P1
        AND ka.AKT_OZNAKA = sa.AKT_OZNAKA
        AND sa.AKT_OZNAKA = suv.STR_AKT_OZNAKA
        AND suv.STR_AKT_OZNAKA <> suv.AKT_OZNAKA
        AND suv.AKT_OZNAKA <> ''
        AND suv.AKT_VERZIJA IS NOT NULL;

    SELECT * FROM NEMANJA_DOKUMENT;
END




            """)




            # Connect to ArangoDB

            arango_client = ArangoClient(hosts=arango_host)

            db = arango_client.db(arango_database)




            # Drop collections if they already exist

            if db.has_collection('slozena_aktivnost'):

                db.delete_collection('slozena_aktivnost')




            if db.has_collection('verzija'):

                db.delete_collection('verzija')




            if db.has_collection('aktivnosti'):

                db.delete_collection('aktivnosti')




            if db.has_collection('containsEdges'):

                db.delete_collection('containsEdges')




            # Create collections

            slozene_aktivnosti = db.create_collection('slozena_aktivnost', edge=False)

            verzije = db.create_collection('verzija', edge=False)

            aktivnosti = db.create_collection('aktivnosti', edge=False)

            contains_edges = db.create_collection('containsEdges', edge=True)




            # Execute the MySQL procedure to populate the temporary table

            mysql_cursor.callproc('nemanja_dokument', [slozena_aktivnost])




            # Fetch data from the temporary table

            mysql_cursor.execute("SELECT * FROM NEMANJA_DOKUMENT")

            nemanja_dokument_rows = mysql_cursor.fetchall()




            # Map of MySQL IDs to ArangoDB document IDs

            document_id_map = {}




            # Insert complex products

            for row in nemanja_dokument_rows:

                slozena_aktivnost = {

                    'AKT_OZNAKA': row[0],

                    'AKT_NAZIV': row[1],


                }

                document_id = slozene_aktivnosti.insert(slozena_aktivnost, return_new=True)['_id']

                document_id_map[(row[0], row[1])] = document_id




            # Insert versions and software products

            for row in nemanja_dokument_rows:

                slozena_aktivnost_id = document_id_map[(row[0], row[1])]




                verzija = {

                    'SA_AKT_OZNAKA': row[2],

                    'SA_AKT_VERZIJA': row[3],

                }

                document_id = verzije.insert(verzija, return_new=True)['_id']

                verzija_id = document_id  # Save the version_id separately

                document_id_map[(row[2], row[3])] = verzija_id




                aktivnost = {

                    'SUV_STR_AKT_OZNAKA': row[4],

                    'SUV_AKT_OZNAKA': row[5],

                    'SUV_AKT_VERZIJA': row[6],


                }

                document_id = aktivnosti.insert(aktivnost, return_new=True)['_id']

                aktivnosti_id = document_id  # Save the software_product_id separately

                document_id_map[(row[4], row[5], row[6])] = aktivnosti_id




                # Create edges

                contains_edges.insert(

                    {

                        '_from': slozena_aktivnost_id,

                        '_to': verzija_id

                    }

                )




                contains_edges.insert(

                    {

                        '_from': verzija_id,

                        '_to': aktivnosti_id

                    }

                )




            print('Data imported successfully.')

    def nina_dokument(self):
        dialog = QtWidgets.QDialog(self)
        dialog.setWindowTitle("Nina Graf")

        layout = QtWidgets.QFormLayout()
        verzijaLineEdit = QtWidgets.QLineEdit()
        kataloskiBrojLineEdit = QtWidgets.QLineEdit()
        layout.addRow("Verzija:", verzijaLineEdit)
        layout.addRow("Kataloski Broj:", kataloskiBrojLineEdit)

        submitButton = QtWidgets.QPushButton("Submit")
        submitButton.clicked.connect(dialog.accept)
        layout.addRow(submitButton)

        dialog.setLayout(layout)
        dialog.exec_()

        if dialog.result() == QtWidgets.QDialog.Accepted:
            verzija = verzijaLineEdit.text()
            kataloski_broj = kataloskiBrojLineEdit.text()

    def dusan_dokument(self):
        dialog = QtWidgets.QDialog(self)
        dialog.setWindowTitle("Dusan Graf")

        layout = QtWidgets.QFormLayout()
        verzijaLineEdit = QtWidgets.QLineEdit()
        kataloskiBrojLineEdit = QtWidgets.QLineEdit()
        layout.addRow("Verzija:", verzijaLineEdit)
        layout.addRow("Kataloski Broj:", kataloskiBrojLineEdit)

        submitButton = QtWidgets.QPushButton("Submit")
        submitButton.clicked.connect(dialog.accept)
        layout.addRow(submitButton)

        dialog.setLayout(layout)
        dialog.exec_()

        if dialog.result() == QtWidgets.QDialog.Accepted:
            verzija = verzijaLineEdit.text()
            kataloski_broj = kataloskiBrojLineEdit.text()

    def lena_dokument(self):
        dialog = QtWidgets.QDialog(self)
        dialog.setWindowTitle("Lena Graf")

        layout = QtWidgets.QFormLayout()
        verzijaLineEdit = QtWidgets.QLineEdit()
        kataloskiBrojLineEdit = QtWidgets.QLineEdit()
        layout.addRow("Verzija:", verzijaLineEdit)
        layout.addRow("Kataloski Broj:", kataloskiBrojLineEdit)

        submitButton = QtWidgets.QPushButton("Submit")
        submitButton.clicked.connect(dialog.accept)
        layout.addRow(submitButton)

        dialog.setLayout(layout)
        dialog.exec_()

        if dialog.result() == QtWidgets.QDialog.Accepted:
            verzija = verzijaLineEdit.text()
            kataloski_broj = kataloskiBrojLineEdit.text()

    def mihajlo_dokument(self):
        dialog = QtWidgets.QDialog(self)
        dialog.setWindowTitle("Mihajlo Graf")

        layout = QtWidgets.QFormLayout()
        idKompanijeLineEdit = QtWidgets.QLineEdit()
        drzavaLineEdit = QtWidgets.QLineEdit()
        layout.addRow("Id kompanije:", idKompanijeLineEdit)
        layout.addRow("Drzava:", drzavaLineEdit)

        submitButton = QtWidgets.QPushButton("Submit")
        submitButton.clicked.connect(dialog.accept)
        layout.addRow(submitButton)

        dialog.setLayout(layout)
        dialog.exec_()

        if dialog.result() == QtWidgets.QDialog.Accepted:
            id_kompanije = idKompanijeLineEdit.text()
            drzava = drzavaLineEdit.text()

            cnx = mysql.connector.connect(user='root', password='root', host='localhost', database='nosqlprimerbaza')
            cursor = cnx.cursor()
            cursor.callproc('mihajlo_dokument', [id_kompanije, drzava])

            cursor.execute("SELECT * from MIHAJLO_DOKUMENT")

            result = cursor.fetchall()

            ps = {}

            mongo_doc = {

                'poslovni_subjekat': [],
                

            }



            for row in result:

                ps = {

                    'drzava': row[0],

                    'id_kompanije': row[2],

                    'naziv_kompanije': row[3],

                    'sediste_kompanije': [

                        {

                            'drzava_kompanije': row[0],

                            'id_naselja': row[5],

                            'naseljeno_mesto_naziv': row[6]

                        }

                    ],
                    'org_jed' : [{
                        'id_kompanije_org' : row[2],
                        'id_org_jed' : row[7],
                        'naziv_org_jed' : row[8],
                        'registar_delatnosti': [

                            {

                                'oznaka_drzave': row[0],

                                'id_reg_del': row[9],

                                'naziv_reg_del': row[10]

                            }
                        ]
                }]

                }
                

                mongo_doc['poslovni_subjekat'].append(ps)


            # Close the MySQL connection

            doc2 = {

                "_id": ObjectId(),

                "naslov": f"Pregled strukture registrovanih delatnosti za kompaniju {id_kompanije} u drzavi {drzava}",

                "podaci": {

                    "Registrovane delatnosti": list(mongo_doc.values())

                }

            }

            client = MongoClient("mongodb://localhost:27017")

            db = client["nosqlprimerbaza"]

            collection = db["mihajlo_dokument"]

            collection.insert_one(doc2)

            print(doc2)

            cursor.close()

            cnx.close()

            dokument = "Mihajlo Dokument"

            self.nosql_area.docChosen(collection, dokument)
