from PySide2 import QtWidgets
from bson import ObjectId
from pymongo import MongoClient
import mysql.connector

class OptionsDialog(QtWidgets.QDialog):
    def __init__(self, nosql_area):
        super().__init__()
        self.setWindowTitle("Choose an Option")
        self.nosql_area = nosql_area
        options = [
            "Bogdan Dokument",
            "Nemanja Dokument",
            "Nina Dokument",
            "Dusan Dokument",
            "Lena Dokument",
            "Mihajlo Dokument"
        ]

        self.optionComboBox = QtWidgets.QComboBox()
        self.optionComboBox.addItems(options)

        submitButton = QtWidgets.QPushButton("Submit")
        submitButton.clicked.connect(self.handle_submit)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.optionComboBox)
        layout.addWidget(submitButton)
        self.setLayout(layout)

    def handle_submit(self):
        selected_option = self.optionComboBox.currentText()

        if selected_option == "Bogdan Dokument":
            self.bogdan_dokument()
        elif selected_option == "Nemanja Dokument":
            self.nemanja_dokument()
        elif selected_option == "Nina Dokument":
            self.nina_dokument()
        elif selected_option == "Dusan Dokument":
            self.dusan_dokument()
        elif selected_option == "Lena Dokument":
            self.lena_dokument()
        elif selected_option == "Mihajlo Dokument":
            self.mihajlo_dokument()

    def bogdan_dokument(self):
        dialog = QtWidgets.QDialog(self)
        dialog.setWindowTitle("Bogdan Dokument")

        layout = QtWidgets.QFormLayout()
        verzijaLineEdit = QtWidgets.QLineEdit()
        kataloskiBrojLineEdit = QtWidgets.QLineEdit()
        layout.addRow("Verzija:", verzijaLineEdit)
        layout.addRow("Kataloski Broj:", kataloskiBrojLineEdit)

        submitButton = QtWidgets.QPushButton("Submit")
        submitButton.clicked.connect(dialog.accept)
        layout.addRow(submitButton)

        dialog.setLayout(layout)
        dialog.exec_()

        if dialog.result() == QtWidgets.QDialog.Accepted:
            verzija = verzijaLineEdit.text()
            kataloski_broj = kataloskiBrojLineEdit.text()

            # Perform actions with verzija and kataloski_broj inputs
            cnx = mysql.connector.connect(user='root', password='root', host='localhost', database='nosql')
            cursor = cnx.cursor()
            cursor.execute("DROP PROCEDURE IF EXISTS bogdan_dokument")

            # Create the procedure
            cursor.execute("""
            CREATE DEFINER = `root`@`localhost` PROCEDURE `bogdan_dokument` (IN ver INT, katbr char(10), drzava char(4))
            BEGIN 
                DROP TEMPORARY TABLE IF EXISTS BOGDAN_DOKUMENT;

                CREATE TEMPORARY TABLE BOGDAN_DOKUMENT(
 SP_DR_OZNAKA char(4), 
 SP_POS_PS_ID int, 
 SP_SPR_KATBR char(10),
 SP_SPR_TIP char(1),
 SP_SPR_NAZIV varchar(120), 
 VER_VER_SOF_DR_OZNAKA char(4), 
 VER_VER_POS_PS_ID int, 
 VER_SPR_KATBR char(10), 
 VER_SPR_TIP char(1), 
 VER_VER_OZNAKA int, 
 SA_SOF_DR_OZNAKA char(4), 
 SA_POS_PS_ID int, 
 SA_KON_SPR_KATBR char(10), 
 SA_KON_SPR_TIP char(1), 
 SA_KON_VER_OZNAKA int, 
 SA_KONF_ID int, 
 SA_SASTAV_RBR smallint, 
 SA_VER_SOF_DR_OZNAKA char(4), 
 SA_VER_POS_PS_ID int, 
 SA_SPR_KATBR char(10), 
 SA_SPR_TIP char(1), 
 SA_VER_OZNAKA int
 );
    INSERT INTO BOGDAN_DOKUMENT(
    SP_DR_OZNAKA, 
    SP_POS_PS_ID, 
    SP_SPR_KATBR,
    SP_SPR_TIP,
    SP_SPR_NAZIV,

    VER_VER_SOF_DR_OZNAKA, 
    VER_VER_POS_PS_ID, 
    VER_SPR_KATBR, 
    VER_SPR_TIP, 
    VER_VER_OZNAKA, 
    
    SA_SOF_DR_OZNAKA, 
    SA_POS_PS_ID, 
    SA_KON_SPR_KATBR, 
    SA_KON_SPR_TIP, 
    SA_KON_VER_OZNAKA, 
    SA_KONF_ID, 
    SA_SASTAV_RBR, 
    SA_VER_SOF_DR_OZNAKA, 
    SA_VER_POS_PS_ID, 
    SA_SPR_KATBR, 
    SA_SPR_TIP, 
    SA_VER_OZNAKA
    )
SELECT
    SP.DR_OZNAKA,
    SP.POS_PS_ID,
    SP.SPR_KATBR,
    SP.SPR_TIP,
    SP.SPR_NAZIV,
    V.SOF_DR_OZNAKA,
    V.POS_PS_ID,
    V.SPR_KATBR,
    V.SPR_TIP,
    V.VER_OZNAKA,
    S.SOF_DR_OZNAKA,
    S.POS_PS_ID,
    S.KON_SPR_KATBR,
    S.KON_SPR_TIP,
    S.KON_VER_OZNAKA,
    S.KONF_ID,
    S.SASTAV_RBR,
    S.VER_SOF_DR_OZNAKA,
    S.VER_POS_PS_ID,
    S.SPR_KATBR,
    S.SPR_TIP,
    S.VER_OZNAKA
FROM softverski_proizvod SP, verzija V, sastav S
WHERE SP.DR_OZNAKA = V.SOF_DR_OZNAKA
    AND SP.POS_PS_ID = V.POS_PS_ID
    AND SP.SPR_KATBR = V.SPR_KATBR
    AND SP.SPR_TIP = V.SPR_TIP
    AND V.SOF_DR_OZNAKA = S.SOF_DR_OZNAKA
    AND V.POS_PS_ID = S.POS_PS_ID
    AND V.SPR_KATBR = S.KON_SPR_KATBR
    AND V.SPR_TIP = S.KON_SPR_TIP
    AND V.VER_OZNAKA = S.KON_VER_OZNAKA
    AND SP.SPR_KATBR = katbr
    AND V.VER_OZNAKA = ver
    AND EXISTS (
        SELECT 1
        FROM sastav S2
        WHERE S2.SOF_DR_OZNAKA = SP.DR_OZNAKA
            AND S2.POS_PS_ID = SP.POS_PS_ID
            AND S2.KON_SPR_KATBR = SP.SPR_KATBR
            AND S2.KON_SPR_TIP = SP.SPR_TIP
            AND S2.KON_VER_OZNAKA = V.VER_OZNAKA
    );
            END
            """)

            # Execute the stored procedure to retrieve the data
            cursor.callproc('bogdan_dokument', [int(verzija), kataloski_broj]) 
            cursor.execute("SELECT * from BOGDAN_DOKUMENT")
            result = cursor.fetchall()


            # Transform the data into the MongoDB document structure
            software_product = {}

            mongo_doc = {
                'softverski_proizvodi': []
            }

            for row in result:
                software_product = {
                    'drzava': row[0],
                    'verzija': row[9],
                    'kataloski broj': row[2],
                    'Tip': row[3],
                    'naziv': row[4],
                    'Sastav': [
                        {
                            'kataloski broj': row[7],
                            'Tip': row[8],
                            'Redni broj': row[16]
                        }
                    ]
                }
                mongo_doc['softverski_proizvodi'].append(software_product)

            # Close the MySQL connection
            doc2 = {
                "_id": ObjectId(),
                "naslov": f"Prikaz struktura slozenih softverskih proizvoda sa verzijom {verzija} i kataloskim brojem {kataloski_broj}",
                "podaci": {
                    "Softverski proizvodi": list(mongo_doc.values())
                }
            }

            client = MongoClient("mongodb://localhost:27017")
            db = client["test"]
            collection = db["softverski_proizvod"]

            collection.insert_one(doc2)
            print(doc2)
            cursor.close()
            cnx.close()
            dokument = "Bogdan Dokument"
            self.nosql_area.docChosen(collection, dokument)

    def nemanja_dokument(self):
        dialog = QtWidgets.QDialog(self)
        dialog.setWindowTitle("Nemanja Dokument")

        layout = QtWidgets.QFormLayout()
        drzavaLineEdit = QtWidgets.QLineEdit()
        kompanijaLineEdit = QtWidgets.QLineEdit()
        softverskiProcesLineEdit = QtWidgets.QLineEdit()
        verzijaLineEdit = QtWidgets.QLineEdit()
        layout.addRow("Drzava:", drzavaLineEdit)
        layout.addRow("Kompanija:", kompanijaLineEdit)
        layout.addRow("Softverski proces:", softverskiProcesLineEdit)
        layout.addRow("Verzija:", verzijaLineEdit)

        submitButton = QtWidgets.QPushButton("Submit")
        submitButton.clicked.connect(dialog.accept)
        layout.addRow(submitButton)

        dialog.setLayout(layout)
        dialog.exec_()

        if dialog.result() == QtWidgets.QDialog.Accepted:
            drzava = drzavaLineEdit.text()
            kompanija = kompanijaLineEdit.text()
            softverski_proces = softverskiProcesLineEdit.text()
            verzija = verzijaLineEdit.text()

            cnx = mysql.connector.connect(user='root', password='root', host='localhost', database='nosql')

            cursor = cnx.cursor()
            cursor.execute("DROP PROCEDURE IF EXISTS nemanja_dokument")




            # Create the procedure

            cursor.execute("""

            CREATE DEFINER = `root`@`localhost` PROCEDURE `nemanja_dokument` (IN P1 VARCHAR(50), IN P2 INT, IN P3 INT, IN P4 SMALLINT)
BEGIN
    DROP TEMPORARY TABLE IF EXISTS NEMANJA_DOKUMENT;
    
    CREATE TEMPORARY TABLE NEMANJA_DOKUMENT (
        DR_OZNAKA varchar(50),
        PS_ID int,
        PS_NAZIV varchar(120),
        ID_PROCESA int,
        PROC_NAZIV varchar(120),
        STP_DR_OZNAKA varchar(50),
        STP_PS_NAZIV varchar(120),
        STP_ID_PROCESA int,
        STP_AKT_OZNAKA char(2),
        STP_AKT_NAZIV varchar(240),
        STP_AKT_VERZIJA smallint,
        SUV_SLOZENA_AKTIVNOST varchar(2),
        SUV_AKTIVNOST VARCHAR(2),
        
        SUV_VERZIJA smallint,
        SUV_NAZIV_AKTIVNOSTI varchar(240)
    );

    INSERT INTO NEMANJA_DOKUMENT (
        DR_OZNAKA,
        PS_ID,
        PS_NAZIV,
        ID_PROCESA,
        PROC_NAZIV,
        STP_DR_OZNAKA,
        STP_PS_NAZIV,
        STP_ID_PROCESA,
        STP_AKT_OZNAKA,
        STP_AKT_NAZIV,
        STP_AKT_VERZIJA,
        SUV_SLOZENA_AKTIVNOST,
         SUV_AKTIVNOST,
        
        SUV_VERZIJA,
        SUV_NAZIV_AKTIVNOSTI
    )
    SELECT
        str.DR_OZNAKA,
        str.PS_ID,
        (SELECT PS_NAZIV FROM poslovni_subjekat WHERE DR_OZNAKA = str.DR_OZNAKA AND PS_ID = str.PS_ID),
        str.ID_PROCESA,
        (SELECT PROC_NAZIV FROM softverski_proces WHERE DR_OZNAKA = str.DR_OZNAKA AND PS_ID = str.PS_ID AND ID_PROCESA = str.ID_PROCESA),
        str.DR_OZNAKA,
        (SELECT PS_NAZIV FROM poslovni_subjekat WHERE DR_OZNAKA = str.DR_OZNAKA AND PS_ID = str.PS_ID),
        str.ID_PROCESA,
        str.AKT_OZNAKA,
        (SELECT AKT_NAZIV FROM katalog_aktivnosti WHERE AKT_OZNAKA = str.AKT_OZNAKA),
        str.AKT_VERZIJA,
        SUV.STR_AKT_OZNAKA,
        suv.AKT_OZNAKA,
        suv.AKT_VERZIJA,
        (SELECT AKT_NAZIV FROM katalog_aktivnosti WHERE AKT_OZNAKA = suv.AKT_OZNAKA)
    FROM
        struktura_procesa str,
        sastav__u_verziji suv
    WHERE
        str.DR_OZNAKA = P1
        AND str.PS_ID = P2
        AND str.ID_PROCESA = P3
        AND str.AKT_VERZIJA = P4
        AND str.AKT_OZNAKA = suv.STR_AKT_OZNAKA
        AND str.AKT_VERZIJA = suv.AKT_VERZIJA
        AND str.AKT_OZNAKA <> suv.AKT_OZNAKA
        AND str.AKT_OZNAKA <> ''
        AND suv.AKT_OZNAKA <> '';

    SELECT * FROM NEMANJA_DOKUMENT;

END

            """)



            # Execute the stored procedure to retrieve the data

            cursor.callproc('nemanja_dokument', [drzava, kompanija,softverski_proces,verzija])

            cursor.execute("SELECT * from NEMANJA_DOKUMENT")

            result = cursor.fetchall()





            # Transform the data into the MongoDB document structure

            software_proces = {}




            mongo_doc = {

                'struktura_procesa': []

            }




            for row in result:

                software_proces = {
                    

                    'aktivnost': row[8],

                    'naziv aktivnosti': row[9],

                    'Sastav u verziji': [

                        {

                            'aktivnost u sastavu': row[12],

                            'naziv aktivnosti u sastavu': row[14],

                        }

                    ]

                }

                mongo_doc['struktura_procesa'].append(software_proces)




            # Close the MySQL connection

            doc2 = {

                "_id": ObjectId(),

                "naslov": f"Prikaz strukture softverskog procesa za naziv kompanije {kompanija} za proces  {softverski_proces} sa strukturom aktivnosti u verziji {verzija}" ,

                "podaci": {

                    "Struktura procesa": list(mongo_doc.values())

                }

            }




            client = MongoClient("mongodb://localhost:27017")

            db = client["nosql"]

            collection = db["nemanja_dokument"]




            collection.insert_one(doc2)

            print(doc2)

            cursor.close()

            cnx.close()
            dokument = "Nemanja Dokument"

            self.nosql_area.docChosen(collection, dokument)

    def nina_dokument(self):
        dialog = QtWidgets.QDialog(self)
        dialog.setWindowTitle("Nina Dokument")

        layout = QtWidgets.QFormLayout()
        verzijaLineEdit = QtWidgets.QLineEdit()
        kataloskiBrojLineEdit = QtWidgets.QLineEdit()
        layout.addRow("Verzija:", verzijaLineEdit)
        layout.addRow("Kataloski Broj:", kataloskiBrojLineEdit)

        submitButton = QtWidgets.QPushButton("Submit")
        submitButton.clicked.connect(dialog.accept)
        layout.addRow(submitButton)

        dialog.setLayout(layout)
        dialog.exec_()

        if dialog.result() == QtWidgets.QDialog.Accepted:
            verzija = verzijaLineEdit.text()
            kataloski_broj = kataloskiBrojLineEdit.text()

    def dusan_dokument(self):
        dialog = QtWidgets.QDialog(self)
        dialog.setWindowTitle("Dusan Dokument")

        layout = QtWidgets.QFormLayout()
        kompanijaLineEdit = QtWidgets.QLineEdit()
        layout.addRow("Kompanija:", kompanijaLineEdit)

        submitButton = QtWidgets.QPushButton("Submit")
        submitButton.clicked.connect(dialog.accept)
        layout.addRow(submitButton)

        dialog.setLayout(layout)
        dialog.exec_()

        if dialog.result() == QtWidgets.QDialog.Accepted:
            kompanija = kompanijaLineEdit.text()

        

            cnx = mysql.connector.connect(user='root', password='root', host='localhost', database='nosql')

            cursor = cnx.cursor()



            cursor.callproc('dusan_dokument', [kompanija])

            cursor.execute("SELECT * from DUSAN_DOKUMENT")

            result = cursor.fetchall()



            lista_softverskih_proizvoda = {}




            mongo_doc = {

                'lista_softverskih_proizvoda': []

            }


            for row in result:

                lista_softverskih_proizvoda = {

                    'Kataloski Broj': row[5],

                    'Tip': row[6],

                    'Naziv Proizvoda': row[7],

                    'Verzija': [

                        {
                            'Oznaka verzije': row[14]
                        }

                    ]

                }

                mongo_doc['lista_softverskih_proizvoda'].append(lista_softverskih_proizvoda)



            doc2 = {

                "_id": ObjectId(),

                "naslov": f"Prikaz kataloga slozenih softverskih proizvoda za proizvodjaca {kompanija}",

                "podaci": {

                    "Lista Softverskih Proizvoda": list(mongo_doc.values())

                }

            }




            client = MongoClient("mongodb://localhost:27017")

            db = client["nosql"]

            collection = db["dusan_dokument"]




            collection.insert_one(doc2)

            print(doc2)

            cursor.close()

            cnx.close()



    def lena_dokument(self):
        dialog = QtWidgets.QDialog(self)
        dialog.setWindowTitle("Lena Dokument")

        layout = QtWidgets.QFormLayout()
        verzijaLineEdit = QtWidgets.QLineEdit()
        kataloskiBrojLineEdit = QtWidgets.QLineEdit()
        layout.addRow("Verzija:", verzijaLineEdit)
        layout.addRow("Kataloski Broj:", kataloskiBrojLineEdit)

        submitButton = QtWidgets.QPushButton("Submit")
        submitButton.clicked.connect(dialog.accept)
        layout.addRow(submitButton)

        dialog.setLayout(layout)
        dialog.exec_()

        if dialog.result() == QtWidgets.QDialog.Accepted:
            verzija = verzijaLineEdit.text()
            kataloski_broj = kataloskiBrojLineEdit.text()

    def mihajlo_dokument(self):
        dialog = QtWidgets.QDialog(self)
        dialog.setWindowTitle("Mihajlo Dokument")

        layout = QtWidgets.QFormLayout()
        idKompanijeLineEdit = QtWidgets.QLineEdit()
        drzavaLineEdit = QtWidgets.QLineEdit()
        layout.addRow("Id kompanije:", idKompanijeLineEdit)
        layout.addRow("Drzava:", drzavaLineEdit)

        submitButton = QtWidgets.QPushButton("Submit")
        submitButton.clicked.connect(dialog.accept)
        layout.addRow(submitButton)

        dialog.setLayout(layout)
        dialog.exec_()

        if dialog.result() == QtWidgets.QDialog.Accepted:
            id_kompanije = idKompanijeLineEdit.text()
            drzava = drzavaLineEdit.text()

            cnx = mysql.connector.connect(user='root', password='root', host='localhost', database='nosqlprimerbaza')
            cursor = cnx.cursor()
            cursor.callproc('mihajlo_dokument', [id_kompanije, drzava])

            cursor.execute("SELECT * from MIHAJLO_DOKUMENT")

            result = cursor.fetchall()

            ps = {}

            mongo_doc = {

                'poslovni_subjekat': [],
                

            }



            for row in result:

                ps = {

                    'drzava': row[0],

                    'id_kompanije': row[2],

                    'naziv_kompanije': row[3],

                    'sediste_kompanije': [

                        {

                            'drzava_kompanije': row[0],

                            'id_naselja': row[5],

                            'naseljeno_mesto_naziv': row[6]

                        }

                    ],
                    'org_jed' : [{
                        'id_kompanije_org' : row[2],
                        'id_org_jed' : row[7],
                        'naziv_org_jed' : row[8],
                        'registar_delatnosti': [

                            {

                                'oznaka_drzave': row[0],

                                'id_reg_del': row[9],

                                'naziv_reg_del': row[10]

                            }
                        ]
                }]

                }
                

                mongo_doc['poslovni_subjekat'].append(ps)


            # Close the MySQL connection

            doc2 = {

                "_id": ObjectId(),

                "naslov": f"Pregled strukture registrovanih delatnosti za kompaniju {id_kompanije} u drzavi {drzava}",

                "podaci": {

                    "Registrovane delatnosti": list(mongo_doc.values())

                }

            }

            client = MongoClient("mongodb://localhost:27017")

            db = client["nosqlprimerbaza"]

            collection = db["mihajlo_dokument"]

            collection.insert_one(doc2)

            print(doc2)

            cursor.close()

            cnx.close()

            dokument = "Mihajlo Dokument"

            self.nosql_area.docChosen(collection, dokument)
