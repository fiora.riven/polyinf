from PySide2.QtGui import QStandardItemModel, QStandardItem
from PySide2.QtGui import QStandardItemModel, QStandardItem
from PySide2 import QtWidgets, QtCore
import json
from pymongo import MongoClient
import pymongo.errors
from nosql_workspace.options_dialog import OptionsDialog
from nosql_workspace.arango_options_dialog import ArangoOptionsDialog


class NoSqlDockWidget(QtWidgets.QDockWidget):
    clicked = QtCore.Signal(str, str)

    id = 0

    def __init__(self, title, nosql_area):
        super().__init__(title)
        self.setFeatures(QtWidgets.QDockWidget.NoDockWidgetFeatures)

        self.nosql_area = nosql_area
        self.options_dialog = OptionsDialog(nosql_area)
        self.arango_options_dialog = ArangoOptionsDialog()

        self.mongoTreeModel = QStandardItemModel()

        self.tabWidget = QtWidgets.QTabWidget()

        # MongoDB tab
        self.mongoTab = QtWidgets.QWidget()
        self.mongoLayout = QtWidgets.QVBoxLayout(self.mongoTab)
        self.mongoLayout.setContentsMargins(0, 0, 0, 0)

        self.loadMongoDbButton = QtWidgets.QPushButton("Učitaj bazu")
        self.loadMongoDbButton.setStyle(QtWidgets.QStyleFactory.create("Fusion"))
        self.loadMongoDbButton.setProperty("class", "QPushButton")
        self.loadMongoDbButton.setProperty("borders", "True")
        self.loadMongoDbButton.setProperty("borderRadius", "10px")

        self.mongoLayout.addWidget(self.loadMongoDbButton)


        self.createDatabaseButton = QtWidgets.QPushButton("Kreiraj kolekciju")
        self.mongoLayout.addWidget(self.createDatabaseButton)


        self.mongoDocuments = QtWidgets.QPushButton("Personalni Dokument")
        self.mongoDocuments.setStyle(QtWidgets.QStyleFactory.create("Fusion"))
        self.mongoDocuments.setProperty("class", "QPushButton")
        self.mongoDocuments.setProperty("borders", "True")
        self.mongoDocuments.setProperty("borderRadius", "10px")

        self.mongoLayout.addWidget(self.mongoDocuments)


        self.recnik = {}

        self.mongoTreeView = QtWidgets.QTreeView()
        self.mongoTreeView.setHeaderHidden(True)
        self.mongoTreeView.setModel(self.mongoTreeModel)
        self.mongoLayout.addWidget(self.mongoTreeView)





    

        self.tabWidget.addTab(self.mongoTab, "MongoDB")

        # ArangoDB tab
        self.arangoTab = QtWidgets.QWidget()
        self.tabWidget.addTab(self.arangoTab, "ArangoDB")
        self.arangoLayout = QtWidgets.QVBoxLayout(self.arangoTab)


        self.loadArangoDbButton = QtWidgets.QPushButton("Pokreni ArangoDB")
                # # sytle
        self.loadArangoDbButton.setStyle(QtWidgets.QStyleFactory.create("Fusion"))  # Use Fusion style
        self.loadArangoDbButton.setProperty("class", "QPushButton")
        self.loadArangoDbButton.setProperty("borders", "True")
        self.loadArangoDbButton.setProperty("borderRadius", "10px")

        self.arangoLayout.addWidget(self.loadArangoDbButton)

        self.arangoDoc = QtWidgets.QPushButton("Personalni Graf")
                # # sytle
        self.arangoDoc.setStyle(QtWidgets.QStyleFactory.create("Fusion"))  # Use Fusion style
        self.arangoDoc.setProperty("class", "QPushButton")
        self.arangoDoc.setProperty("borders", "True")
        self.arangoDoc.setProperty("borderRadius", "10px")

        self.arangoLayout.addWidget(self.arangoDoc)





        self.recnik = {}
        self.arangoTreeModel = QStandardItemModel()
        self.arangoTreeView = QtWidgets.QTreeView()
        self.arangoTreeView.setHeaderHidden(True)
        self.arangoTreeView.setModel(self.arangoTreeModel)
        self.arangoLayout.addWidget(self.arangoTreeView)



        self.setWidget(self.tabWidget)



        self.loadArangoDbButton.clicked.connect(self.startArango)




        self.loadMongoDbButton.clicked.connect(self.loadMongoDatabases)
        self.createDatabaseButton.clicked.connect(self.createCollection)


        self.tabWidget.currentChanged.connect(self.tabChanged)

        self.mongoDocuments.clicked.connect(self.open_dialog)
        self.arangoDoc.clicked.connect(self.open_arango_dialog)

        

    def open_dialog(self):
        self.options_dialog.exec_()

    def open_arango_dialog(self):
        self.arango_options_dialog.exec_()



    def loadMongoDatabases(self):
        # Učitavanje konfiguracionog fajla
        with open('mongodb_config.json') as config_file:
            config = json.load(config_file)

        # Dobijanje URI iz konfiguracije
        uri = config['host']

        # Povezivanje na MongoDB server
        try:
            client = MongoClient(uri)
            databases = client.list_database_names()
            self.mongoTreeModel.clear()

            for database in databases:
                database_item = QStandardItem(database)
                self.mongoTreeModel.appendRow(database_item)

                # Učitavanje kolekcija za trenutnu bazu podataka
                db = client[database]
                collections = db.list_collection_names()

                for collection in collections:
                    collection_item = QStandardItem(collection)
                    database_item.appendRow(collection_item)

        except pymongo.errors.ConnectionFailure:
            print("Greška prilikom povezivanja na MongoDB server")

        # Emitovanje signala da je baza izabrana
        selected_database = "selected_database"
        self.clicked.emit("mongodb", selected_database)

        self.recnik[self.id] = self.mongoTreeView
        self.id += 1
        for index, self.mongoTreeView in self.recnik.items():
            self.mongoTreeView.clicked.connect(lambda: self.treeClicked(index))

    def treeClicked(self, index):
        
        tree_view = self.recnik[index]
        for i in tree_view.selectedIndexes():
            collection = i.data()
            database = i.parent().data()
        self.nosql_area.onClicked(collection, database)
        




    def deleteDatabase(self):
        # Povezivanje na MongoDB server
        with open('mongodb_config.json') as config_file:
            config = json.load(config_file)
        uri = config['host']
        client = MongoClient(uri)

        # Izlistavanje svih baza
        databases = client.list_database_names()

        # Prikazivanje dijaloga za odabir baze za brisanje
        database_name, ok = QtWidgets.QInputDialog.getItem(
            self, "Brisanje baze", "Odaberite bazu za brisanje:", databases, 0, False
        )

        if ok and database_name:
            # Potvrda brisanja
            confirm_message = f"Da li ste sigurni da želite da obrišete bazu '{database_name}'?"
            confirm_dialog = QtWidgets.QMessageBox.question(
                self, "Potvrda brisanja", confirm_message,
                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No
            )

            if confirm_dialog == QtWidgets.QMessageBox.Yes:
                # Brisanje baze
                client.drop_database(database_name)
                print(f"Baza podataka '{database_name}' je uspešno obrisana.")

                # Uklanjanje baze iz TreeView-a
                for i in range(self.mongoTreeModel.rowCount()):
                    item = self.mongoTreeModel.item(i)
                    if item.text() == database_name:
                        self.mongoTreeModel.removeRow(i)
                        break
        else:
            print("Odabir baze za brisanje je obavezan.")


    def createCollection(self):
        # Povezivanje na MongoDB server
        with open('mongodb_config.json') as config_file:
            config = json.load(config_file)
        uri = config['host']
        client = MongoClient(uri)

        
        database_list = client.list_database_names()
        database_list.append("KREIRAJ NOVU BAZU")

        database_name, ok = QtWidgets.QInputDialog.getItem(
            self, "Odabir baze", "Odaberite bazu:", database_list, 0, False
        )

        if ok and database_name:
            if database_name == "KREIRAJ NOVU BAZU":
                # Kreiranje nove baze
                new_database_name, ok = QtWidgets.QInputDialog.getText(
                    self, "Kreiranje nove baze", "Unesite naziv nove baze podataka:"
                )
                if ok and new_database_name:
                    # Provera da li nova baza već postoji
                    if new_database_name in client.list_database_names():
                        print(f"Baza podataka '{new_database_name}' već postoji.")
                    else:
                        # Kreiranje nove kolekcije unutar nove baze
                        db = client[new_database_name]
                        collection_name, ok = QtWidgets.QInputDialog.getText(
                            self, "Kreiranje nove kolekcije", "Unesite naziv nove kolekcije:"
                        )
                        if ok and collection_name:
                            try:
                                db.create_collection(collection_name)
                                print(f"Kolekcija '{collection_name}' je uspešno kreirana u bazi '{new_database_name}'.")
                                # Dodavanje nove baze u Tree View
                                database_item = QStandardItem(new_database_name)
                                self.mongoTreeModel.appendRow(database_item)
                            except pymongo.errors.OperationFailure as e:
                                print(f"Greška pri kreiranju kolekcije: {str(e)}")
                        else:
                            print("Unos naziva nove kolekcije je obavezan.")
                else:
                    print("Unos naziva nove baze podataka je obavezan.")
            else:
                # Odabir postojeće baze
                collection_name, ok = QtWidgets.QInputDialog.getText(
                    self, "Kreiranje nove kolekcije", "Unesite naziv nove kolekcije:"
                )
                if ok and collection_name:
                    try:
                        db = client[database_name]
                        db.create_collection(collection_name)
                        print(f"Kolekcija '{collection_name}' je uspešno kreirana u bazi '{database_name}'.")
                    except pymongo.errors.OperationFailure as e:
                        print(f"Greška pri kreiranju kolekcije: {str(e)}")
                else:
                    print("Unos naziva nove kolekcije je obavezan.") 


    def tabChanged(self):

        if self.tabWidget.tabText(self.tabWidget.currentIndex()) == "MongoDB":
            self.nosql_area.layout().itemAt(0).widget().setVisible(True)
            self.nosql_area.layout().itemAt(1).widget().setVisible(False)
        else:
            self.nosql_area.layout().itemAt(1).widget().setVisible(True)
            self.nosql_area.layout().itemAt(0).widget().setVisible(False)




    def startArango(self):
        self.nosql_area.startArango()



    