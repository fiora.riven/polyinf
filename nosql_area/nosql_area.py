from PySide2 import QtWidgets
from pymongo import MongoClient
from PySide2.QtCore import Qt, QUrl
from PySide2.QtWidgets import QTableWidget, QTableWidgetItem
from PySide2.QtWebEngineWidgets import QWebEngineView
import json

class NoSqlArea(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()




        self.arangoTabWidget = QtWidgets.QTabWidget()
        self.arangoTabWidget.setTabsClosable(True)
        self.arangoTabWidget.tabCloseRequested.connect(self.closeTab)

        self.arangoTabWidget.setVisible(False)
        


        layout = QtWidgets.QVBoxLayout()        
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        self.setLayout(layout)
        self.mongoTabWidget = QtWidgets.QTabWidget()
        self.mongoTabWidget.setTabsClosable(True)
        self.mongoTabWidget.tabCloseRequested.connect(self.closeTab)

        layout.addWidget(self.mongoTabWidget)
        layout.addWidget(self.arangoTabWidget)
        

    
    def onClicked(self, col, database):
        client = MongoClient('mongodb://localhost:27017/')
        db = client[database]
        collection = db[col]

        widget = QtWidgets.QWidget()
        layout = QtWidgets.QVBoxLayout()
        widget.setLayout(layout)

        data = []
        for document in collection.find():
            # Convert ObjectId to string
            document['_id'] = str(document['_id'])
            data.append(document)

        # Serialize data to JSON
        json_data = json.dumps(data)

        # Create a text edit widget to display the JSON document
        self.text_edit = QtWidgets.QTextEdit()
        self.text_edit.setReadOnly(True)
        self.text_edit.setText(json_data)
        
        self.button = QtWidgets.QPushButton("JSON View")
        self.button.clicked.connect(self.switch_tables)

 
        self.table = QTableWidget()
        cursor = collection.find()
        data = list(cursor)



        self.table.setRowCount(len(data))
        self.table.setColumnCount(len(data[0]))

        for row, item in enumerate(data):
            for column, value in enumerate(item.values()):
                self.table.setItem(row, column, QTableWidgetItem(str(value)))

        

        self.table.setHorizontalHeaderLabels(item.keys())
        self.table.horizontalHeader().setStretchLastSection(True)
        self.table.verticalHeader().setVisible(False)
        self.table.setEditTriggers(QTableWidget.NoEditTriggers)
        self.table.setSelectionMode(QTableWidget.NoSelection)
        self.table.setFocusPolicy(Qt.NoFocus)

        self.text_edit.setVisible(False)
        layout.addWidget(self.button)
        layout.addWidget(self.table)
        layout.addWidget(self.text_edit)

        self.mongoTabWidget.addTab(widget, "" + str(col))


    def docChosen(self, collection, dokument):

        widget = QtWidgets.QWidget()
        layout = QtWidgets.QVBoxLayout()
        widget.setLayout(layout)

        data = []
        for document in collection.find():
            # Convert ObjectId to string
            document['_id'] = str(document['_id'])
            data.append(document)

        # Serialize data to JSON
        json_data = json.dumps(data)

        # Create a text edit widget to display the JSON document
        self.text_edit = QtWidgets.QTextEdit()
        self.text_edit.setReadOnly(True)
        self.text_edit.setText(json_data)
        
        self.button = QtWidgets.QPushButton("JSON View")
        self.button.clicked.connect(self.switch_tables)

 
        self.table = QTableWidget()
        cursor = collection.find()
        data = list(cursor)



        self.table.setRowCount(len(data))
        self.table.setColumnCount(len(data[0]))

        for row, item in enumerate(data):
            for column, value in enumerate(item.values()):
                self.table.setItem(row, column, QTableWidgetItem(str(value)))

        

        self.table.setHorizontalHeaderLabels(item.keys())
        self.table.horizontalHeader().setStretchLastSection(True)
        self.table.verticalHeader().setVisible(False)
        self.table.setEditTriggers(QTableWidget.NoEditTriggers)
        self.table.setSelectionMode(QTableWidget.NoSelection)
        self.table.setFocusPolicy(Qt.NoFocus)

        self.text_edit.setVisible(False)
        layout.addWidget(self.button)
        layout.addWidget(self.table)
        layout.addWidget(self.text_edit)

        self.mongoTabWidget.addTab(widget, "" + str(dokument))


    def closeTab(self, index):
        self.mongoTabWidget.removeTab(index)

    def switch_tables(self):
        if self.table.isVisible() is True:
            self.table.setVisible(False)
            self.text_edit.setVisible(True)
            self.button.setText("Table View")
        else:
            self.table.setVisible(True)
            self.text_edit.setVisible(False)
            self.button.setText("JSON View")




    def startArango(self):
        web_view = QWebEngineView()
        web_view.load(QUrl("http://127.0.0.1:8529"))
        self.arangoTabWidget.addTab(web_view, "ArangoDB")




