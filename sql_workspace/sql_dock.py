from PySide2 import QtWidgets, QtCore, QtGui
import mysql.connector
from sql_workspace import config
from sql_workspace.dialog import DatabaseSelectionDialog
import mysql.connector
from pymongo import MongoClient
from PySide2.QtWidgets import QApplication, QMessageBox, QLabel
import re
from ui.create_table_window import CreateTableWindow

class SqlDockWidget(QtWidgets.QDockWidget):
    id = 0
    
    def __init__(self, title, area):
        super().__init__(title)
        self.setFeatures(QtWidgets.QDockWidget.NoDockWidgetFeatures)
        self.sql_area = area
        self.selected_table = None
        self.create_table_window = None
        
        self.central_widget = QtWidgets.QWidget(self)
        self.setWidget(self.central_widget)

        self.vertical_layout = QtWidgets.QVBoxLayout()
        self.vertical_layout = QtWidgets.QVBoxLayout()
        self.vertical_layout.setContentsMargins(0, 0, 0, 0)
        self.central_widget.setLayout(self.vertical_layout)

        self.button_layout = QtWidgets.QHBoxLayout()
        self.button_table_layout = QtWidgets.QHBoxLayout()

        self.getAllDatabasesButton = QtWidgets.QPushButton("Ucitaj Bazu", self.central_widget)
        # # sytle
        self.getAllDatabasesButton.setStyle(QtWidgets.QStyleFactory.create("Fusion"))  # Use Fusion style
        self.getAllDatabasesButton.setProperty("class", "QPushButton")
        self.getAllDatabasesButton.setProperty("borders", "True")
        self.getAllDatabasesButton.setProperty("borderRadius", "10px")
        # #hover
        # self.getAllDatabasesButton.installEventFilter(self)
        # #layout
        self.vertical_layout.addWidget(self.getAllDatabasesButton)

        '''self.createButton = QtWidgets.QPushButton("Kreiraj bazu", self.central_widget)
        self.createButton.setStyle(QtWidgets.QStyleFactory.create("Fusion"))  # Use Fusion style
        self.createButton.setProperty("class", "QPushButton")
        self.createButton.setProperty("borders", "True")
        self.createButton.setProperty("borderRadius", "10px")
        #hover
        self.createButton.installEventFilter(self)
        self.button_layout.addWidget(self.createButton)

        self.deleteButton = QtWidgets.QPushButton("Obrisi bazu", self.central_widget)
        #style
        self.deleteButton.setStyle(QtWidgets.QStyleFactory.create("Fusion"))  # Use Fusion style
        self.deleteButton.setProperty("class", "QPushButton")
        self.deleteButton.setProperty("borders", "True")
        self.deleteButton.setProperty("borderRadius", "10px")
        #hover
        self.deleteButton.installEventFilter(self)
        self.button_layout.addWidget(self.deleteButton)'''
        
        # #Create table button
        # self.createTableButton = QtWidgets.QPushButton("Kreiraj tabelu", self.central_widget)
        # #style
        # self.createTableButton.setStyle(QtWidgets.QStyleFactory.create("Fusion"))  # Use Fusion style
        # self.createTableButton.setProperty("class", "QPushButton")
        # self.createTableButton.setProperty("borders", "True")
        # self.createTableButton.setProperty("borderRadius", "10px")
        # self.disable_create_table_button()

        #hover
        # self.createTableButton.installEventFilter(self)
        # self.button_table_layout.addWidget(self.createTableButton)
        
        # #Delete table button
        # self.deleteTableButton = QtWidgets.QPushButton("Obrisi tabelu", self.central_widget)
        # #style
        # self.deleteTableButton.setStyle(QtWidgets.QStyleFactory.create("Fusion"))  # Use Fusion style
        # self.deleteTableButton.setProperty("class", "QPushButton")
        # self.deleteTableButton.setProperty("borders", "True")
        # self.deleteTableButton.setProperty("borderRadius", "10px")
        # self.disable_delete_table_button()
        # #hover
        # self.deleteTableButton.installEventFilter(self)
        # self.button_table_layout.addWidget(self.deleteTableButton)

        self.vertical_layout.addLayout(self.button_layout)
        self.vertical_layout.addLayout(self.button_table_layout)

        self.tabWidget = QtWidgets.QTabWidget()
        self.tabWidget.setTabsClosable(True)
        self.tabWidget.tabCloseRequested.connect(self.closeTab)
        self.vertical_layout.addWidget(self.tabWidget)
        self.recnik = {}

        self.getAllDatabasesButton.clicked.connect(self.openDialog)
        #self.deleteButton.clicked.connect(self.deleteButtonClicked)
        #self.createButton.clicked.connect(self.createDatabase)
        
        # self.createTableButton.clicked.connect(self.createTableMysql)
        # self.deleteTableButton.clicked.connect(self.deleteTableMysql)
        

    # def eventFilter(self, obj, event):
    #     # if obj is self.getAllDatabasesButton:
    #         if event.type() == QtCore.QEvent.HoverEnter:
    #             obj.setStyleSheet("background-color: #72c272;")
    #         elif event.type() == QtCore.QEvent.HoverLeave:
    #             obj.setStyleSheet("")

    #         return super().eventFilter(obj, event)
        
    def openDialog(self):
        dialog = DatabaseSelectionDialog()
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.selected_database = dialog.selectedDatabase()
            self.loadTables()
            # self.enable_create_table_button()


    def createDatabase(self):
        dialog = QtWidgets.QInputDialog(self)
        dialog.setWindowTitle("Kreiranje baze")
        dialog.setLabelText("Unesite ime nove baze:")
        dialog.setOkButtonText("Kreiraj")
        dialog.setCancelButtonText("Otkaži")
        dialog.setInputMode(QtWidgets.QInputDialog.TextInput)

        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            new_database_name = dialog.textValue().strip()

            if new_database_name:
                myconfig = config.load_config("myconfig.json")

                connection = mysql.connector.connect(
                    host=myconfig["host"],
                    user=myconfig["user"],
                    password=myconfig["password"]
                )
                cursor = connection.cursor()

                # Provera da li baza sa istim imenom već postoji
                cursor.execute("SHOW DATABASES")
                existing_databases = cursor.fetchall()
                existing_databases = [db[0] for db in existing_databases]

                if new_database_name in existing_databases:
                    QtWidgets.QMessageBox.warning(
                        self,
                        "Greška",
                        f"Baza podataka sa imenom '{new_database_name}' već postoji."
                    )
                else:
                    query = f"CREATE DATABASE {new_database_name}"
                    cursor.execute(query)
                    connection.commit()

                    cursor.close()
                    connection.close()

                    print("Baza podataka uspešno kreirana!")
            else:
                print("Ime nove baze nije uneto.")



    def deleteButtonClicked(self):
        dialog = DatabaseSelectionDialog()
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            selected_database = dialog.selectedDatabase()

            # Provera da li je korisnik siguran da želi da obriše bazu podataka
            reply = QtWidgets.QMessageBox.question(
                self,
                "Brisanje baze podataka",
                f"Da li ste sigurni da želite da obrišete bazu podataka '{selected_database}'?",
                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No
            )

            if reply == QtWidgets.QMessageBox.Yes:
                myconfig = config.load_config("myconfig.json")

                connection = mysql.connector.connect(
                    host=myconfig["host"],
                    user=myconfig["user"],
                    password=myconfig["password"]
                )
                cursor = connection.cursor()

                query = f"DROP DATABASE {selected_database}"
                cursor.execute(query)
                connection.commit()

                cursor.close()
                connection.close()

                self.tabWidget.clear()

                # Ponovno prikazivanje padajućeg menija sa dostupnim bazama podataka
                # self.openDialog()

                print("Baza podataka uspešno obrisana!")
            else:
                print("Brisanje baze podataka otkazano!")
        
    def replace_invalid_characters_from_table_name(self, table_name):
        pattern = r"[^\w\d]+"
        table_name = re.sub(pattern, "_", table_name)
        return table_name.strip('_')

    def createTableMysql(self):
        if not self.create_table_window:
            self.create_table_window = CreateTableWindow()
            self.create_table_window.signals.finished.connect(self.handle_create_table_window_finished)
        self.create_table_window.show()

    def handle_create_table_window_finished(self):
        self.create_table_window = None
        self.closeTab(self.tabWidget.currentIndex())
        self.loadTables()


    def deleteTableMysql(self):
        msg_box = QMessageBox()
        msg_box.setWindowTitle("Brisanje tabele")
        msg_box.setMinimumSize(400, 200)
        msg_box.setWindowIcon(QtGui.QIcon("resources/icons/mysql_icon.png"))
        msg_box.setText(f"Da li ste sigurni da zelite da obrisete tabelu {self.selected_table}")
        msg_box.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        original_image = QtGui.QImage("resources/icons/table_remove_icon.png")
        target_size = QtCore.QSize(50, 50)
        resized_image = original_image.scaled(target_size, QtCore.Qt.AspectRatioMode.KeepAspectRatio)
        pixmap = QtGui.QPixmap.fromImage(resized_image)
        msg_box.setIconPixmap(pixmap)

        button_clicked = msg_box.exec_()
        if button_clicked == QMessageBox.Yes:
            myconfig = config.load_config("myconfig.json")
            connection = mysql.connector.connect(
                host=myconfig["host"],
                user=myconfig["user"],
                password=myconfig["password"],
                db = self.selected_database
            )

            cursor = connection.cursor()
            drop_table_query = f"DROP TABLE IF EXISTS {self.selected_table}"
            cursor.execute(drop_table_query)
            connection.commit()
            cursor.close()
            connection.close()
            self.closeTab(self.tabWidget.currentIndex())
            self.loadTables()
            tab_name = self.selected_database + '/' + self.selected_table
            self.sql_area.close_table_tab_by_name(tab_name)
        else:
            msg_box.reject()


    def loadTables(self):
        # self.enable_create_table_button()
        self.tree_view = QtWidgets.QTreeView()
        self.tree_view.setHeaderHidden(True)
        self.tree_model = QtGui.QStandardItemModel()
        root_item = self.tree_model.invisibleRootItem()
        self.tree_view.setModel(self.tree_model)
        self.tabWidget.addTab(self.tree_view, self.selected_database)
        self.tabWidget.setCurrentWidget(self.tree_view)

        myconfig = config.load_config("myconfig.json")

        connection = mysql.connector.connect(
            host=myconfig["host"],
            user=myconfig["user"],
            password=myconfig["password"],
            db = self.selected_database
        )
        cursor = connection.cursor()
        cursor.execute("SHOW TABLES")


        tables = cursor.fetchall()

        

        for table in tables:
            table_name = table[0]
            item = QtGui.QStandardItem(table_name)
            root_item.appendRow(item)

        cursor.close()
        connection.close()

        self.recnik[self.id] = self.tree_view
        self.id += 1
        for index, self.tree_view in self.recnik.items():
            self.tree_view.clicked.connect(lambda: self.treeClicked(index))

    def treeClicked(self, index):
        
        tree_view = self.recnik[index]
        for i in tree_view.selectedIndexes():
            table = i.data()
            self.selected_table = table
            # self.enable_delete_table_button()
        self.sql_area.onClicked(table, self.selected_database)
        tree_view.clearSelection()

    def closeTab(self, index):
        database_name = self.tabWidget.tabText(index)
        self.tabWidget.removeTab(index)
        
        #TODO: Kada se zatvori tab baze, teba zatvoriti sve tabove tabela iz te baze 
        self.sql_area.close_all_table_tabs_by_database_name(database_name)
    
        # if self.tabWidget.count() == 0:
        #     self.disable_create_table_button()
        #     self.disable_delete_table_button()
        
    def enable_create_table_button(self):
        self.createTableButton.setEnabled(True)
        palette = self.createTableButton.palette()
        palette.setColor(QtGui.QPalette.Button, QtGui.QColor(255, 255, 255))
        self.createTableButton.setPalette(palette)
        
    def disable_create_table_button(self):
        self.createTableButton.setEnabled(False)
        palette = self.createTableButton.palette()
        palette.setColor(QtGui.QPalette.Button, QtGui.QColor(192, 192, 192))
        self.createTableButton.setPalette(palette)
        
    def enable_delete_table_button(self):
        self.deleteTableButton.setEnabled(True)
        palette = self.deleteTableButton.palette()
        palette.setColor(QtGui.QPalette.Button, QtGui.QColor(255, 255, 255))
        self.deleteTableButton.setPalette(palette)
        
    def disable_delete_table_button(self):
        self.deleteTableButton.setEnabled(False)
        palette = self.deleteTableButton.palette()
        palette.setColor(QtGui.QPalette.Button, QtGui.QColor(192, 192, 192))
        self.deleteTableButton.setPalette(palette)
