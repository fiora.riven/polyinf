from PySide2 import QtWidgets, QtGui
import mysql.connector
from sql_workspace import config

class DatabaseSelectionDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Database Selection")
        
        self.setWindowIcon(QtGui.QIcon("resources/icons/app-icon2.png"))

        layout = QtWidgets.QVBoxLayout(self)

        self.database_combo = QtWidgets.QComboBox()

        self.loadDatabases()

        self.ok_button = QtWidgets.QPushButton("OK")
        self.ok_button.clicked.connect(self.accept)
        label = QtWidgets.QLabel()
        label.setText("Odaberite bazu podataka:")

        layout.addWidget(label)
        layout.addWidget(self.database_combo)
        layout.addWidget(self.ok_button)

        self.setLayout(layout)

    def loadDatabases(self):
        self.database_combo.clear()

        myconfig = config.load_config("myconfig.json")

        connection = mysql.connector.connect(
            host=myconfig["host"],
            user=myconfig["user"],
            password=myconfig["password"]
        )
        cursor = connection.cursor()

        cursor.execute("SHOW DATABASES")

        databases = cursor.fetchall()

        for database in databases:
            self.database_combo.addItem(database[0])

        cursor.close()
        connection.close()
    
    def selectedDatabase(self):
        return self.database_combo.currentText()
