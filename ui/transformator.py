from PyQt5.QtWidgets import QDialog, QVBoxLayout, QLabel, QComboBox, QPushButton
import pymysql
from pymongo import MongoClient
import json
import bson
from bson import ObjectId
import datetime


class Transformator(QDialog):
    def __init__(self, parent=None):
        super(Transformator, self).__init__(parent)
        self.setWindowTitle("Transformacija MySQL baze u MongoDB")
        self.setLayout(QVBoxLayout())

        self.label_baza = QLabel("Izaberite MySQL bazu:")
        self.layout().addWidget(self.label_baza)

        self.combo_box_baza = QComboBox()
        self.layout().addWidget(self.combo_box_baza)

        self.label_tabela = QLabel("Izaberite tabelu za kreiranje u MongoDB bazi:")
        self.layout().addWidget(self.label_tabela)

        self.combo_box_tabela = QComboBox()
        self.layout().addWidget(self.combo_box_tabela)

        self.button = QPushButton("Pokreni transformaciju")
        self.button.clicked.connect(self.pokreni_transformaciju)
        self.layout().addWidget(self.button)

        self.mysql_connection = pymysql.connect(
            host='localhost',  # Promeniti ako je potrebno
            user='root',
            password='root'
        )

        self.mongo_client = MongoClient('mongodb://localhost:27017/')  # Promeniti ako je potrebno

        self.ucitaj_mysql_baze()
        self.combo_box_baza.currentIndexChanged.connect(self.ucitaj_mysql_tabele)

    def ucitaj_mysql_baze(self):
        self.combo_box_baza.clear()

        mysql_cursor = self.mysql_connection.cursor()
        mysql_cursor.execute("SHOW DATABASES")

        mysql_baze = [rezultat[0] for rezultat in mysql_cursor.fetchall()]
        self.combo_box_baza.addItems(mysql_baze)

        # Dodaj poziv metode za ucitavanje tabela
        self.ucitaj_mysql_tabele()

    def ucitaj_mysql_tabele(self):
        self.combo_box_tabela.clear()

        mysql_baza = self.combo_box_baza.currentText()
        if not mysql_baza:
            return

        mysql_connection = pymysql.connect(
            host='localhost',  # Promeniti ako je potrebno
            user='root',
            password='root',
            database=mysql_baza
        )

        mysql_cursor = mysql_connection.cursor()
        mysql_cursor.execute("SHOW TABLES")
        mysql_tabele = [rezultat[0] for rezultat in mysql_cursor.fetchall()]
        self.combo_box_tabela.addItems(mysql_tabele)

    def pokreni_transformaciju(self):
        mysql_baza = self.combo_box_baza.currentText()
        mysql_tabela = self.combo_box_tabela.currentText()

        mysql_connection = pymysql.connect(
            host='localhost',  # Promeniti ako je potrebno
            user='root',
            password='root',
            database=mysql_baza
        )

        mongo_db = self.mongo_client[mysql_baza]

        mysql_cursor = mysql_connection.cursor()
        mysql_cursor.execute("CALL mihajlo_dokument(123)")  # Izvršavanje procedure za popunjavanje privremene tabele

        # Preuzimanje podataka iz privremene tabele
        mysql_cursor.execute("SELECT * FROM MIHAJLO_DOKUMENT")
        rezultat = mysql_cursor.fetchall()

        mongo_kolekcija = mongo_db[mysql_tabela]
        kolekcija_rezultati = []
        for red in rezultat:
            dokument = {}
            for i, polje in enumerate(mysql_cursor.description):
                if isinstance(red[i], ObjectId):
                    dokument[polje[0]] = str(red[i])
                elif isinstance(red[i], datetime.date):
                    dokument[polje[0]] = red[i].strftime('%Y-%m-%d')  # Konverzija u string
                else:
                    dokument[polje[0]] = red[i]
        mongo_kolekcija.insert_one(dokument)
        kolekcija_rezultati.append(dokument)

        with open('result.json', 'w') as f:
            json.dump(kolekcija_rezultati, f, default=str, indent=4, separators=(',', ': '))

        self.label_baza.setText("Transformacija završena! Rezultati su zapisani u result.json.")
        self.combo_box_tabela.clear()

