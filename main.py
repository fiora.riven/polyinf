

from PySide2 import QtWidgets, QtGui, QtCore
from PySide2.QtWidgets import QApplication, QMessageBox, QLabel
import sys
import json
from plugin_framework.plugin_registry import PluginRegistry
from ui.main_window import MainWindow
import os

def _load_configuration(path="configuration.json"):
    with open(path, "r", encoding="utf-8") as fp:
        config = json.load(fp)
        return config

def pokreni_aplikaciju(config):
    application = QtWidgets.QApplication.instance()

    if application is None:
        application = QtWidgets.QApplication(sys.argv)

    main_window = MainWindow(config)
    plugin_registry = PluginRegistry("plugins", main_window)

    main_window.add_plugin_registry(plugin_registry)

    main_window.show()
    sys.exit(application.exec_())

def prikazi_message_box(config):
    app = QApplication.instance()
    if app is None:
        app = QApplication([])

    msg_box = QMessageBox()
    msg_box.setWindowTitle("Polyinf")
    msg_box.setWindowIcon(QtGui.QIcon("resources/icons/app-icon2.png"))
    msg_box.setMinimumSize(400, 200)
    msg_box.setText("Pokrenuli ste Poliglotski rukovalac informacionim resursima.\nDa li želite da nastavite?")


    image_label = QLabel()


    original_image = QtGui.QImage("resources/icons/app-icon2.png")


    target_size = QtCore.QSize(50, 50)
    resized_image = original_image.scaled(target_size, QtCore.Qt.AspectRatioMode.KeepAspectRatio)


    pixmap = QtGui.QPixmap.fromImage(resized_image)

    msg_box.setIconPixmap(pixmap)



    image_label.setFixedSize(0, 0)


    image_label.setAlignment(QtCore.Qt.AlignCenter)

    msg_box.layout().addWidget(image_label)


    msg_box.setStandardButtons(QMessageBox.Yes | QMessageBox.No)


    button_clicked = msg_box.exec_()
    if button_clicked == QMessageBox.Yes:
        pokreni_aplikaciju(config)
    else:
        msg_box.reject()

    app.quit()

if __name__ == "__main__":
    config = _load_configuration()
    prikazi_message_box(config)
